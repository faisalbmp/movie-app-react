import axios from "axios";

let api = axios.create({
  baseURL: "https://api.themoviedb.org/3/",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});

export default api;
