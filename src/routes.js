/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Genres from "features/Genres/Genres";
import MoviesRoute from "features/Movies/MoviesRouter";

var routes = [
  {
    path: "/genres",
    name: "Genres",
    icon: "ni ni-bullet-list-67 text-red",
    component: Genres,
    layout: "/admin"
  },
  {
    path: "/movies",
    name: "Movies",
    icon: "ni ni-tv-2 text-primary",
    component: MoviesRoute,
    layout: "/admin"
  },
];
export default routes;
