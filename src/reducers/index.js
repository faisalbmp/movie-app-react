import { combineReducers } from 'redux';

import { default as globalReducer } from './globalSlice';
import genresReducer from "../features/Genres/genreSlice";
import moviesReducer from "../features/Movies/moviesSlice";

export default combineReducers({
  global: globalReducer,
  genres: genresReducer,
  movies: moviesReducer,
});
