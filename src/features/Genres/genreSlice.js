import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice
} from '@reduxjs/toolkit';
import { getGenres } from './genresApi';

const genresAdapter = createEntityAdapter();


export const fetchGenreData = createAsyncThunk(
  'genres/get',
  async () => {
    try {
      const response = await getGenres();
      return response.data.genres
    } catch (err) {
      throw err;
    }
  }
)
/* const initialState = {
  loading: 'idle',
  currentRequestId: undefined,
  error: null,
  maps:[]
} */


const initialState = genresAdapter.getInitialState({
  loading: 'idle',
  currentRequestId: undefined,
  error: null
})

export const genreSlice = createSlice({
  name: 'genres',
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [fetchGenreData.pending]: (state, action) => {
      state.loading = 'pending'
      state.currentRequestId = action.meta.requestId
    },
    [fetchGenreData.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
        state.error = null;
        genresAdapter.setAll(state, action.payload)
      }
    },
    [fetchGenreData.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.payload && action.payload.message ? action.payload.message : action.message
        state.currentRequestId = undefined
      }
    }
  }
});

const { reducer } = genreSlice;
export default reducer;

export const {
  selectById: selectGenreById,
  selectIds: selectGenreIds,
  selectEntities: selectGenreEntities,
  selectAll: selectAllGenre,
  selectTotal: selectTotalGenre
} = genresAdapter.getSelectors((state) => state.genres);