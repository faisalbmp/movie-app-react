import api from "services/api";

export const getGenres = () => {
  return api.get('/genre/movie/list',{
    params:{
      api_key:"2fccde01a371b106b09a241d6d1d5b49"
    }
  });
};