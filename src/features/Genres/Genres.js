import React, { useEffect } from "react";
import Header from "components/Headers/Header";
import Container from "reactstrap/lib/Container";
import { useDispatch, useSelector } from "react-redux";
import { fetchGenreData, selectAllGenre } from "./genreSlice";

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
const Genres = () => {

  const dispatch = useDispatch();
  const allGenresData = useSelector(selectAllGenre)
  useEffect(() => {
    dispatch(fetchGenreData());

  }, [dispatch])
  return (
    <>
      <Header />
      <Container className="mt--7" fluid>
        <DataTable value={allGenresData}>
          <Column field="id" header="Id" sortable></Column>
          <Column field="name" header="Name" sortable></Column>
        </DataTable>
      </Container>
    </>
  )
}

export default Genres;