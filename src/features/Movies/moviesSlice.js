import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice
} from '@reduxjs/toolkit';
import { getDetailMovies, getMovies } from './moviesApi';

const adapter = createEntityAdapter();


export const fetchMovieData = createAsyncThunk(
  'movies/get',
  async ({ page }, { rejectWithValue }) => {
    try {
      const response = await getMovies({ page });
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return rejectWithValue(err.response.data);
    }
  }
)

export const fetchMovieDetail = createAsyncThunk(
  'movies/detail',
  async ({ id }, { rejectWithValue }) => {
    try {
      const response = await getDetailMovies({ id });
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return rejectWithValue(err.response.data);
    }
  }
)

const initialState = adapter.getInitialState({
  loading: 'idle',
  currentRequestId: undefined,
  error: null,
  totalPage: 0,
  totalResults: 0,
  movie:{},
})

export const movieSlice = createSlice({
  name: 'movies',
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [fetchMovieData.pending]: (state, action) => {
      state.loading = 'pending'
      state.currentRequestId = action.meta.requestId
    },
    [fetchMovieData.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
        state.error = null;
        state.totalPage = action.payload.total_pages
        state.totalResults = action.payload.total_results
        adapter.setAll(state, action.payload.results)
      }
    },
    [fetchMovieData.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.payload && action.payload.message ? action.payload.message : action.message
        state.currentRequestId = undefined
      }
    },
    [fetchMovieDetail.pending]: (state, action) => {
      state.loading = 'pending'
      state.currentRequestId = action.meta.requestId
    },
    [fetchMovieDetail.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
        state.error = null;
        state.movie = action.payload
      }
    },
    [fetchMovieDetail.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.payload && action.payload.message ? action.payload.message : action.message
        state.currentRequestId = undefined
      }
    },
  }
});

const { reducer } = movieSlice;
export default reducer;

export const {
  selectById: selectMovieById,
  selectIds: selectMovieIds,
  selectEntities: selectMovieEntities,
  selectAll: selectAllMovie,
  selectTotal: selectTotalMovie
} = adapter.getSelectors((state) => state.movies);