import api from "services/api";

export const getMovies = ({ page }) => {
  return api.get(`/movie/upcoming/`, {
    params: {
      api_key: "2fccde01a371b106b09a241d6d1d5b49",
      page
    }
  });
};


export const getDetailMovies = ({ id }) => {
  return api.get(`/movie/${id}`, {
    params: {
      api_key: "2fccde01a371b106b09a241d6d1d5b49",
    }
  });
};