import React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import MoviesDetail from "./Detail/MoviesDetail";
import Movies from "./Movies";

const MoviesRoute = () => {
  
  const { url } = useRouteMatch();
  return (
    <Switch>
      <Route exact path={`${url}/`} component={Movies} />
      <Route path={`${url}/:id`} component={MoviesDetail} />
    </Switch>
  );
}

export default MoviesRoute;