import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchMovieData, selectAllMovie } from "./moviesSlice";
import Header from "components/Headers/Header";
import Container from "reactstrap/lib/Container";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import classNames from 'classnames';
import { Button } from "primereact/button";
import { useHistory } from "react-router-dom";
import { fetchGenreData } from "features/Genres/genreSlice";
import { selectAllGenre } from "features/Genres/genreSlice";
const Movies = () => {
  let history = useHistory();

  const dispatch = useDispatch();
  const allMovieData = useSelector(selectAllMovie)
  const allGenreData = useSelector(selectAllGenre)
  const { loading, totalResults } = useSelector((state) => state.movies);

  const [first, setfirst] = useState(1)
  const [last, setlast] = useState(15)

  const countryBodyTemplate = (code) => {
    return (
      <React.Fragment>
        <span className="p-column-title">Country</span>
        <img src="showcase/demo/images/flag_placeholder.png" onError={(e) => e.target.src = 'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} alt={code} className={classNames('flag', 'flag-' + code)} />
      </React.Fragment>
    );
  }

  const actionBodyTemplate = ({ id }) => {
    return (
      <Button label="Detail" onClick={() => history.push(`movies/${id}`)} className="p-button-link" />
    )
  }

  const genreBodyTemplate = ({ genre_ids }) => {
    const genres = genre_ids.map(id => allGenreData.find(genre => id === genre.id).name).join(", ")
    console.log('zip',genres);
    return (
      <div>{genres}</div>
    )
  }

  useEffect(() => {
    dispatch(fetchMovieData({ page: 1 }));
    dispatch(fetchGenreData());
  }, [dispatch])


  const onPage = (event) => {
    //imitate delay of a backend call
    const startIndex = event.first;
    const endIndex = event.first + event.rows;
    dispatch(fetchMovieData(({ page: event.page + 1 })));

    setfirst(startIndex);
    setlast(endIndex);
    // setProducts(datasource.slice(startIndex, endIndex));
  }
  return (
    <>
      <Header />
      <Container className="mt--7" fluid>
        <DataTable value={allMovieData}
          paginator
          loading={loading === "idle" ? false : true}
          totalRecords={totalResults}
          pageLinkSize={5}
          onPage={onPage}
          lazy
          first={first}
          rows={15}
          paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport"
          currentPageReportTemplate={`Showing ${first} to ${last} of ${totalResults} products`}
        >
          <Column field="title" header="Title" sortable></Column>
          <Column field="original_language" header="Origin" body={countryBodyTemplate} sortable></Column>
          <Column field="popularity" header="Popularity" sortable></Column>
          <Column field="vote_average" header="Rating" sortable></Column>
          <Column field="vote_count" header="Total Vote" sortable></Column>
          <Column field="genre_ids" body={genreBodyTemplate} header="Genre" sortable></Column>
          <Column field="id" body={actionBodyTemplate} sortable></Column>
        </DataTable>
      </Container>
    </>
  )
}

export default Movies;