import React, { useEffect } from "react";
import Header from "components/Headers/Header";
import Container from "reactstrap/lib/Container";
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { useDispatch, useSelector } from "react-redux";
import { fetchMovieDetail } from "../moviesSlice";
import { useHistory, useParams } from "react-router-dom";

const MoviesDetail = () => {
  const dispatch = useDispatch()
  const { id } = useParams()
  const history = useHistory()
  const { title, tagline, overview, homepage } = useSelector(state => state.movies.movie)
  useEffect(() => {
    dispatch(fetchMovieDetail({ id }))
  }, [dispatch, id])

  return (
    <>
      <Header />
      <Container className="mt-1" fluid>
        <Card title={title} subTitle={tagline}>
          <div>Summary :</div>
          <div>{overview}</div>
          <div className="d-flex flex-row justify-content-end align-items-center">
            <Button onClick={()=>history.goBack()} icon="pi pi-arrow-left" className="p-button-rounded p-button-secondary p-button-outlined mr-4" />
            <Button label="Go To Link" onClick={() => window.open(homepage)} />
          </div>
        </Card>
      </Container>
    </>
  )
}

export default MoviesDetail