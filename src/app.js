import React from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import store from './store';
import { Provider } from 'react-redux';

import AdminLayout from "layouts/Admin.js";

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route path="/admin" render={props => <AdminLayout {...props} />} />
          <Redirect from="/" to="/admin/genres" />
        </Switch>
      </BrowserRouter>
    </Provider>
  )
}

export default App;